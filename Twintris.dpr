program Twintris;

{$R *.dres}

uses
  Forms,
  Tetris in 'Tetris.pas' {Form1},
  ConstsAndTypes in 'ConstsAndTypes.pas',
  PlayArea in 'PlayArea.pas',
  SoundHandler in 'SoundHandler.pas',
  Bitmaps in 'Bitmaps.pas',
  PlayerInputProvider in 'PlayerInputProvider.pas',
  InitializableList in 'InitializableList.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
