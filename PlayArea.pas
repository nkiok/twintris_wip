unit PlayArea;

interface

uses Vcl.ExtCtrls, Vcl.Graphics, ConstsAndTypes, PlayerInputProvider;

type
  TPlayArea = class(TInterfacedObject)
  private
    DropSpeed: Integer;
    fCountdown: Integer;
    fWho: String;
    function PieceCollided: Boolean;
    function CollidesRight: Boolean;
    function CollidesLeft: Boolean;
    procedure SetCountdown(const Value: Integer);
  public
    DrawPlayArea: TImage;
    DrawScoreArea: TImage;
    Tetrominoes: Array[1..TetrominoesCount] of TetrominoRec;
    CurrentTetromino: XYRec;
    Piece: Array[1..PieceXTot,1..PieceYTot] of Byte;
    TetCount, TetToCount: Word;
    GameOver: Boolean;
    LineEvent: Boolean;
    LineEventFrames: Byte;
    LineEventLines: Array[1..PieceYTot] of Boolean;
    Lines: Longint;
    LastLines: Longint;
    ScoreBGCol: TColor;
    NextTetromino: Byte;
    Score: Longint;
    PlayerInput: TPlayerInputProvider;
    ComboMultiplier: Integer;
    ActivePowerup: TPowerup;
    Opponent: TPlayArea;
    FlipDirection: TFlipDirection;

    constructor Create(const PlayArea, ScoreArea: TImage; const Who: String);
    destructor Destroy; override;
    procedure AssignShape(const ShapeIndex: Integer; const aImage: TImage);
    function ShapeFilledAt(const ShapeNum, ShapeRotation, Y, X: ShortInt): Boolean;
    function CurrentShapeFilledAt(const Y, X: ShortInt): Boolean;
    procedure Restart;
    procedure IncreaseSpeed;
    function PieceDownSide: Byte;
    function PieceLeftSide: Byte;
    function PieceRightSide: Byte;
    function PieceUpSide: Byte;
    function CheckLines: Boolean;
    function PickTetromino: Byte;
    function ReachedBottom: Boolean;
    function DoesNotCollideLeft: Boolean;
    function DoesNotCollideRight: Boolean;
    procedure UpdateCountdown;
    procedure ActivatePowerup;
    procedure ApplyPowerup(const Powerup: TPowerup);

    property Who: String read fWho;
    property CountDown: Integer read fCountDown write SetCountdown;
  end;

implementation

uses WinApi.Windows, Math, SoundHandler;

constructor TPlayArea.Create(const PlayArea, ScoreArea: TImage; const Who: String);
begin
    Opponent:= nil;
    DrawPlayArea:= PlayArea;
    DrawScoreArea:= ScoreArea;
    fWho:= Who;
    DropSpeed:= InitialDropSpeed;

    Tetrominoes[1].Def:= TetrominoStraight;
    Tetrominoes[2].Def:= TetrominoT;
    Tetrominoes[3].Def:= TetrominoCube;
    Tetrominoes[4].Def:= TetrominoZ;
    Tetrominoes[5].Def:= TetrominoS;
    Tetrominoes[6].Def:= TetrominoL;
    Tetrominoes[7].Def:= TetrominoJ;

    NextTetromino:= PickTetromino;

    PlayerInput:= TPlayerInputProvider.Create;

    Restart;
end;

destructor TPlayArea.Destroy;
begin
    PlayerInput.Free;
    inherited;
end;

function TPlayArea.PickTetromino: Byte;
begin
    Result:= Random(TetrominoesCount) + 1;
end;

procedure TPlayArea.AssignShape(const ShapeIndex:Integer; const aImage: TImage);
begin
    Tetrominoes[ShapeIndex].P := aImage.Picture.Bitmap;
end;

procedure TPlayArea.SetCountdown(const Value: Integer);
begin
    fCountDown:= Value;
    if fCountdown < 1 then
    begin
        fCountdown:= DefaultCountdown;
        IncreaseSpeed;
    end
    else
    begin
        fCountDown:= Min(DefaultCountdown, fCountDown);
    end;
end;

function TPlayArea.ShapeFilledAt(const ShapeNum, ShapeRotation, Y, X: ShortInt): Boolean;
begin
    Result:= Tetrominoes[ShapeNum].Def[ShapeRotation, Y, X];
end;

function TPlayArea.CurrentShapeFilledAt(const Y, X: ShortInt): Boolean;
begin
    Result:= ShapeFilledAt(CurrentTetromino.ShapeNum, CurrentTetromino.ShapeRot, Y, X);
end;

procedure TPlayArea.Restart;
begin
    ActivePowerup:= puNone;
    FlipDirection:= fdNone;
    Lines:= 0;
    LastLines:= 0;
    ComboMultiplier:= 0;
    Score:= 0;
    GameOver:= False;
    LineEvent:= False;
    TetToCount:= DropSpeed div TimerInterval;
    fCountdown:= DefaultCountdown;
    TetCount:= 0;
    CurrentTetromino.X:= 5;
    CurrentTetromino.Y:= 0;
    CurrentTetromino.ShapeNum:= PickTetromino;
    CurrentTetromino.ShapeRot:= 1;
    ZeroMemory(@Piece, SizeOf(Piece));
end;

procedure TPlayArea.IncreaseSpeed;
begin
    Dec(DropSpeed, 10);
    TetToCount:= DropSpeed div TimerInterval;
end;

function TPlayArea.PieceCollided:Boolean;
var
    B: Array[1..4] of Byte;
    BX, BY: Byte;
begin
    Result:= False;

    ZeroMemory(@B, Sizeof(B));

    for BX:= 4 downto 1 do
        for BY:= 4 downto 1 do
            if (CurrentShapeFilledAt(BY, BX)) and (B[BX] = 0) then
                B[BX]:= BY;

    if (Piece[CurrentTetromino.X,     B[1] + CurrentTetromino.Y] <> 0) and (B[1] <> 0) then begin Result:= True; Exit; end;
    if (Piece[CurrentTetromino.X + 1, B[2] + CurrentTetromino.Y] <> 0) and (B[2] <> 0) then begin Result:= True; Exit; end;
    if (Piece[CurrentTetromino.X + 2, B[3] + CurrentTetromino.Y] <> 0) and (B[3] <> 0) then begin Result:= True; Exit; end;
    if (Piece[CurrentTetromino.X + 3, B[4] + CurrentTetromino.Y] <> 0) and (B[4] <> 0) then begin Result:= True; Exit; end;
end;

function TPlayArea.CollidesRight: Boolean;
var
    B: Array[1..4] of Byte;
    B1, B2:Byte;
begin
    Result:= False;

    ZeroMemory(@B, Sizeof(B));

    for B1:= 4 downto 1 do
        for B2:= 4 downto 1 do
            if (CurrentShapeFilledAt(B1, B2)) and (B[B1] = 0) then
                B[B1]:= B2;

    if (CurrentTetromino.X     > 1) and (Piece[CurrentTetromino.X + B[1], CurrentTetromino.Y    ] <> 0) and (B[1] <> 0) then begin Result:= True; Exit; end;
    if (CurrentTetromino.X + 1 > 1) and (Piece[CurrentTetromino.X + B[2], CurrentTetromino.Y + 1] <> 0) and (B[2] <> 0) then begin Result:= True; Exit; end;
    if (CurrentTetromino.X + 2 > 1) and (Piece[CurrentTetromino.X + B[3], CurrentTetromino.Y + 2] <> 0) and (B[3] <> 0) then begin Result:= True; Exit; end;
    if                                  (Piece[CurrentTetromino.X + B[4], CurrentTetromino.Y + 3] <> 0) and (B[4] <> 0) then begin Result:= True; Exit; end;
end;

function TPlayArea.CollidesLeft: Boolean;
var
    B: Array[1..4] of Byte;
    B1, B2: Byte;
begin
    Result:= False;

    ZeroMemory(@B, Sizeof(B));

    for B1:= 1 to 4 do
        for B2:= 1 to 4 do
            if (CurrentShapeFilledAt(B1, B2)) and (B[B1] = 0) then
                B[B1]:= B2;

    if (CurrentTetromino.X     > 1) and (Piece[CurrentTetromino.X + B[1] - 2, CurrentTetromino.Y    ] <> 0) and (B[1] <> 0) then begin Result:= True; Exit; end;
    if (CurrentTetromino.X + 1 > 1) and (Piece[CurrentTetromino.X + B[2] - 2, CurrentTetromino.Y + 1] <> 0) and (B[2] <> 0) then begin Result:= True; Exit; end;
    if (CurrentTetromino.X + 2 > 1) and (Piece[CurrentTetromino.X + B[3] - 2, CurrentTetromino.Y + 2] <> 0) and (B[3] <> 0) then begin Result:= True; Exit; end;
    if                                  (Piece[CurrentTetromino.X + B[4] - 2, CurrentTetromino.Y + 3] <> 0) and (B[4] <> 0) then begin Result:= True; Exit; end;
end;

function TPlayArea.PieceLeftSide: Byte;
var
    B: Array[1..4] of Byte;
    BR, BT, BX, BY: Byte;
begin
    ZeroMemory(@B, Sizeof(B));

    for BY:= 1 to 4 do
        for BX:= 1 to 4 do
            if (CurrentShapeFilledAt(BY, BX)) and (B[BY] = 0) then
                B[BY]:= BX;
    BR:= 5;
    for BT:= 1 to 4 do
        if (B[BT] < BR) and (B[BT] <> 0) then
            BR:= B[BT];

    Result:= BR;
end;

function TPlayArea.PieceUpSide: Byte;
var
    B:Array[1..4] of Byte;
    BR, BT, BX, BY: Byte;
begin
    ZeroMemory(@B, Sizeof(B));

    for BX:= 1 to 4 do
        for BY:= 1 to 4 do
            if (CurrentShapeFilledAt(BY, BX)) and (B[BX] = 0) then
                B[BX]:= BY;
    BR:= 5;
    for BT:= 1 to 4 do
        if (B[BT] < BR) and (B[BT] <> 0) then
            BR:= B[BT];

    Result:= BR;
end;

function TPlayArea.PieceDownSide: Byte;
var
    B: Array[1..4] of Byte;
    BR, BT, BX, BY: Byte;
begin
    ZeroMemory(@B, Sizeof(B));

    for BX:= 4 downto 1 do
        for BY:= 4 downto 1 do
            if (CurrentShapeFilledAt(BY, BX)) and (B[BX] = 0) then
                B[BX]:= BY;
    BR:= 0;
    for BT:= 1 to 4 do
        if (B[BT] > BR) and (B[BT] <> 0) then
            BR:= B[BT];

    Result:= BR;
end;

function TPlayArea.PieceRightSide: Byte;
var
    B:Array[1..4] of Byte;
    BR, BT, BX, BY: Byte;
begin
    ZeroMemory(@B, Sizeof(B));

    for BY:= 4 downto 1 do
        for BX:= 4 downto 1 do
            if (CurrentShapeFilledAt(BY, BX)) and (B[BY] = 0) then
                B[BY]:= BX;
    BR:= 0;
    for BT:= 1 to 4 do
        if (B[BT] > BR) and (B[BT] <> 0) then
            BR:= B[BT];

    Result:= BR;
end;

function TPlayArea.CheckLines: Boolean;
var
    X, Y: Word;
    CurrentLines: LongInt;
begin
    Result:= False;

    for X:= 1 to PieceYTot do
        LineEventLines[X]:= True;

    for Y:= 1 to PieceYTot do
        for X:= 1 to PieceXTot do
            if Piece[X,Y] = 0 then
                LineEventLines[Y]:= False;

    CurrentLines:= 0;
    for X:= 1 to PieceYTot do
        if LineEventLines[X] then
        begin
            LineEvent:= True;
            LineEventFrames:= 0;
            for Y:= 1 to PieceYTot do
                if LineEventLines[Y] then
                    Inc(CurrentLines);

            inc(ComboMultiplier);

            case CurrentLines of
                1:  Inc(Score, 100 * ComboMultiplier);
                2:  Inc(Score, 300 * ComboMultiplier);
                3:  Inc(Score, 500 * ComboMultiplier);
                4:  Inc(Score, 800 * ComboMultiplier);
            end;

            Inc(Lines, CurrentLines);
            LastLines:= CurrentLines;

            if FlipDirection <> fdNone then
            begin
                FlipDirection:= fdNone;
            end;

            if Assigned(Opponent) then
            begin
                if ComboMultiplier > Opponent.ComboMultiplier then
                begin
                    if ActivePowerup = puNone then
                    begin
                        ActivePowerup:= TPowerup(Random(Ord(High(TPowerup))) + 1);
                    end;
                end;
            end;

            if CurrentLines = 4 then
                PlaySoundFromResource('Tetris')
            else
                PlaySoundFromResource('Line');

            Countdown:= Countdown + CurrentLines;

            if Lines mod 10 = 0 then
            begin
                if DropSpeed >= 5 then
                begin
                    IncreaseSpeed;
                end;
            end;

            Result:= True;
            Break;
        end;
end;

function TPlayArea.ReachedBottom: Boolean;
begin
    Result:= ((CurrentTetromino.Y + PieceDownSide) - 1 >= PieceYTot) or PieceCollided;
end;

function TPlayArea.DoesNotCollideLeft: Boolean;
begin
    Result:= ((CurrentTetromino.X + PieceLeftSide) - 1 > 1) and (not CollidesLeft);
end;

function TPlayArea.DoesNotCollideRight: Boolean;
begin
    Result:= ((CurrentTetromino.X + PieceRightSide) - 1 < PieceXTot) and (not CollidesRight);
end;

procedure TPlayArea.UpdateCountdown;
begin
    Countdown:= Countdown -1;
end;

procedure TPlayArea.ActivatePowerup;
begin
    case ActivePowerup of
        puSlowdown:
            begin
                Inc(DropSpeed, 10);
            end;
        puTimeFull:
            begin
                CountDown:= DefaultCountdown;
            end;
        else
        begin
            Opponent.ApplyPowerup(ActivePowerup);
        end;
    end;
    ActivePowerup:= puNone;
end;

procedure TPlayArea.ApplyPowerup(const Powerup: TPowerup);
begin
    case Powerup of
        puFlipHorizontal:
            begin
                if FlipDirection = fdVertical then
                begin
                    FlipDirection:= fdBoth;
                end else
                begin
                    FlipDirection:= fdHorizontal;
                end;
            end;

        puFlipVertical:
            begin
                if FlipDirection = fdHorizontal then
                begin
                    FlipDirection:= fdBoth;
                end else
                begin
                    FlipDirection:= fdVertical;
                end;
            end;

        puSpeedup:
            begin
                Dec(DropSpeed, 50);
            end;

        puTimeFlies:
            begin
                CountDown:= CountDown - 5;
            end;
    end;
end;

end.
