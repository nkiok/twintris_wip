unit InitializableList;

interface

uses System.Generics.Collections;

type
    TInitializableList<T> = class(TList<T>)
        constructor Create(const Values: array of T);
    end;

implementation

constructor TInitializableList<T>.Create(const Values: array of T);
var
    Value: T;
begin
    inherited Create;
    for Value in Values do
        Add(Value);
end;

end.
