unit PlayerInputProvider;

interface

uses System.Generics.Collections, ConstsAndTypes;

type
  TPlayerInput = (piNone, piLeft, piRight, piRotate, piDrop, piHardDrop, piPowerup);

  TPlayerInputProvider = class(TInterfacedObject)
  private
    PlayerInputs: TDictionary<TPlayerInput, TList<Integer>>;
    function CheckKeyInput(const PlayerInput: TPlayerInput; const KeyInput: Integer): Boolean; overload;
    function CheckKeyInput(const aListOfKeyboardCodes: TList<Integer>): Boolean; overload;
    function IsKeyDown(key: Word): Boolean;
  public
    constructor Create;
    destructor Destroy; override;
    function BindKeyboardCodes(const aPlayerInput: TPlayerInput; const aListOfKeyboardCodes: TList<Integer>): TPlayerInputProvider;
    function ClearBindings: TPlayerInputProvider;
    function IsLeft: Boolean;
    function IsRight: Boolean;
    function IsDrop: Boolean;
    function IsHardDrop(const KeyInput: Integer): Boolean;
    function IsRotate(const KeyInput: Integer): Boolean;
    function IsPowerup(const KeyInput: Integer): Boolean;
  end;

implementation

uses WinApi.Windows;

constructor TPlayerInputProvider.Create;
begin
    inherited;
    PlayerInputs:= TDictionary<TPlayerInput, TList<Integer>>.Create;
    ClearBindings;
end;

destructor TPlayerInputProvider.Destroy;
begin
    PlayerInputs.Free;
    inherited;
end;

function TPlayerInputProvider.BindKeyboardCodes(const aPlayerInput: TPlayerInput; const aListOfKeyboardCodes: TList<Integer>): TPlayerInputProvider;
begin
    Result:= Self;
    PlayerInputs.Add(aPlayerInput, aListOfKeyboardCodes);
end;

function TPlayerInputProvider.CheckKeyInput(const PlayerInput: TPlayerInput;
  const KeyInput: Integer): Boolean;
begin
    Result:= PlayerInputs[PlayerInput].Contains(KeyInput);
end;

function TPlayerInputProvider.CheckKeyInput(const aListOfKeyboardCodes: TList<Integer>): Boolean;
var
    key: Integer;
begin
    Result:= False;
    for key in aListOfKeyboardCodes do
    begin
        Result:= Result or IsKeyDown(key);
    end;
end;

function TPlayerInputProvider.IsKeyDown(key: Word): Boolean;
var
    state: Integer;
begin
    state:= GetKeyState(key);
    Result := (state and $80 = $80);
end;

function TPlayerInputProvider.ClearBindings: TPlayerInputProvider;
begin
    Result:= Self;
    PlayerInputs.Clear;
    BindKeyboardCodes(piNone, TList<Integer>.Create);
end;

function TPlayerInputProvider.IsLeft: Boolean;
begin
    Result:= CheckKeyInput(PlayerInputs[piLeft]);
end;

function TPlayerInputProvider.IsRight: Boolean;
begin
    Result:= CheckKeyInput(PlayerInputs[piRight]);
end;

function TPlayerInputProvider.IsDrop: Boolean;
begin
    Result:= CheckKeyInput(PlayerInputs[piDrop]);
end;

function TPlayerInputProvider.IsHardDrop(const KeyInput: Integer): Boolean;
begin
    Result:= CheckKeyInput(piHardDrop, KeyInput);
end;

function TPlayerInputProvider.IsRotate(const KeyInput: Integer): Boolean;
begin
    Result:= CheckKeyInput(piRotate, KeyInput);
end;

function TPlayerInputProvider.IsPowerup(const KeyInput: Integer): Boolean;
begin
    Result:= CheckKeyInput(piPowerup, KeyInput);
end;

end.
