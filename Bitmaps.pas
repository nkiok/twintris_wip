unit Bitmaps;

interface

uses Windows, System.Classes, System.SysUtils, Vcl.Graphics, Vcl.ExtCtrls, ConstsAndTypes;

type
    TNumberBitmap = class(TBitmap)
    public
        function Render(const aNumber: Integer; const PadSize: Integer = 0): TBitmap;
    end;

    TBitmapFactory = class(TObject)
        class function GiveMeA24bitBitmap: TBitmap;
        class function GiveMeA32bitBitmap: TBitmap;
        class function GiveMeA24bitBitmapFromResource(const ResourceName: String): TBitmap;
        class function GiveMeA32bitBitmapFromResource(const ResourceName: String): TBitmap;
    end;

    TBitmapHelper = class helper for TBitmap
        function WithWidth(const aWidth: Integer): TBitmap;
        function WithHeight(const aHeight: Integer): TBitmap;
        function Rotate(const Rads: Single; const AdjustSize: Boolean; const BkColor: TColor = clNone): TBitmap;
        function Resize(const maxWidth, maxHeight: integer): TBitmap;
        function Mirror(const X, Y, Width, Height: Integer; const FlipDirection: TFlipDirection): TBitmap;
        function MirrorFrom(const bitmap: TBitmap; const X, Y, Width, Height: Integer; const FlipDirection: TFlipDirection): TBitmap;
        function RenderText(const AText: TStringList; const MaskColor: TColor = clBlack; const AMarginX: Integer = 0; const AMarginY: Integer = 0): TBitmap; overload;
        function RenderText(const AText: String; const MaskColor: TColor = clBlack; const AMarginX: Integer = 0; const AMarginY: Integer = 0): TBitmap; overload;
        function RenderMapOnText(const AText: TStringList; const bmpMap: array of TBitmap; const MaskColor: TColor = clBlack) :TBitmap; overload;
        function RenderMapOnText(const AText: String; const bmpMap: array of TBitmap; const MaskColor: TColor = clBlack) :TBitmap; overload;
        function RenderOn(const Handle: Cardinal; const X, Y, Width, Height: Integer; const RasterOperation: Cardinal = SRCCOPY): TBitmap;
        function RenderTransparentOn(const Cnv: TCanvas; const x, y: Integer; const clTransparent: TColor): TBitmap;
        function Tint(const R, G, B :Word): TBitmap;
        function AutoCrop(iBleeding: Integer; BackColor: TColor): TBitmap;
        function AsImage(const RasterOperation: Cardinal): TImage;
    end;

implementation

uses System.UITypes, System.UIConsts, Math;

type
    TRGBTripleArray = Array[0..MaxInt div SizeOf(integer) - 1] of TRGBTriple;
    PRGBTripleArray = ^TRGBTripleArray;

class function TBitmapFactory.GiveMeA24bitBitmap: TBitmap;
begin
    Result:= TBitmap.Create;
    Result.PixelFormat:= pf24bit;
end;

class function TBitmapFactory.GiveMeA32bitBitmap: TBitmap;
begin
    Result:= TBitmap.Create;
    Result.PixelFormat:= pf32bit;
end;

class function TBitmapFactory.GiveMeA24BitBitmapFromResource(const ResourceName: String): TBitmap;
begin
    Result:= GiveMeA24bitBitmap;
    Result.LoadFromResourceName(HInstance, ResourceName);;
end;

class function TBitmapFactory.GiveMeA32BitBitmapFromResource(const ResourceName: String): TBitmap;
begin
    Result:= GiveMeA32bitBitmap;
    Result.LoadFromResourceName(HInstance, ResourceName);;
end;

function TBitmapHelper.RenderTransparentOn(const Cnv: TCanvas; const x, y: Integer; const clTransparent: TColor): TBitmap;
var
    bmpXOR, bmpAND, bmpINVAND, bmpTarget: TBitmap;
    oldcol: Longint;
begin
    Result:= Self;
    try
        bmpAND:= TBitmapFactory.GiveMeA24bitBitmap
            .WithWidth(Self.Width)
            .WithHeight(Self.Height);

        bmpAND.Monochrome:= True;
        oldcol:= SetBkColor(Self.Canvas.Handle, ColorToRGB(clTransparent));
        BitBlt(
            bmpAND.Canvas.Handle,
            0,
            0,
            Self.Width,
            Self.Height,
            Self.Canvas.Handle,
            0,
            0,
            SRCCOPY);

        SetBkColor(Self.Canvas.Handle, oldcol);

        bmpINVAND:= TBitmapFactory.GiveMeA24bitBitmap
            .WithWidth(Self.Width)
            .WithHeight(Self.Height);

        bmpINVAND.Monochrome:= True;
        BitBlt(
            bmpINVAND.Canvas.Handle,
            0,
            0,
            Self.Width,
            Self.Height,
            bmpAND.Canvas.Handle,
            0,
            0,
            NOTSRCCOPY);

        bmpXOR := TBitmapFactory.GiveMeA24bitBitmap
            .WithWidth(Self.Width)
            .WithHeight(Self.Height);

        BitBlt(
            bmpXOR.Canvas.Handle,
            0,
            0,
            Self.Width,
            Self.Height,
            Self.Canvas.Handle,
            0,
            0,
            SRCCOPY);

        BitBlt(
            bmpXOR.Canvas.Handle,
            0,
            0,
            Self.Width,
            Self.Height,
            bmpINVAND.Canvas.Handle,
            0,
            0,
            SRCAND);

        bmpTarget:= TBitmapFactory.GiveMeA24bitBitmap
            .WithWidth(Self.Width)
            .WithHeight(Self.Height);

        BitBlt(
            bmpTarget.Canvas.Handle,
            0,
            0,
            Self.Width,
            Self.Height,
            Cnv.Handle,
            x,
            y,
            SRCCOPY);

        BitBlt(
            bmpTarget.Canvas.Handle,
            0,
            0,
            Self.Width,
            Self.Height,
            bmpAND.Canvas.Handle,
            0,
            0,
            SRCAND);

        BitBlt(
            bmpTarget.Canvas.Handle,
            0,
            0,
            Self.Width,
            Self.Height,
            bmpXOR.Canvas.Handle,
            0,
            0,
            SRCINVERT);

        BitBlt(
            Cnv.Handle,
            x,
            y,
            Self.Width,
            Self.Height,
            bmpTarget.Canvas.Handle,
            0,
            0,
            SRCCOPY);
    finally
        bmpXOR.Free;
        bmpAND.Free;
        bmpINVAND.Free;
        bmpTarget.Free;
    end;
end;

function TBitmapHelper.Rotate(const Rads: Single; const AdjustSize: Boolean; const BkColor: TColor = clNone): TBitmap;
var
    C: Single;
    S: Single;
    XForm: tagXFORM;
    MemBmp: TBitmap;
begin
    Result:= Self;

    C:= Cos(Rads);
    S:= Sin(Rads);

    XForm.eM11:= C;
    XForm.eM12:= S;
    XForm.eM21:= -S;
    XForm.eM22:= C;

    MemBmp:= TBitmapFactory.GiveMeA24bitBitmap;
    try
        MemBmp.TransparentColor:= Self.TransparentColor;
        MemBmp.TransparentMode:= Self.TransparentMode;
        MemBmp.Transparent:= Self.Transparent;
        MemBmp.Canvas.Brush.Color:= BkColor;
        if AdjustSize then
        begin
            MemBmp.Width:= Round(Self.Width * Abs(C) + Self.Height * Abs(S));
            MemBmp.Height:= Round(Self.Width * Abs(S) + Self.Height * Abs(C));
            XForm.eDx:= (MemBmp.Width - Self.Width * C + Self.Height * S) / 2;
            XForm.eDy:= (MemBmp.Height - Self.Width * S - Self.Height * C) / 2;
        end
        else
        begin
            MemBmp.Width:= Self.Width;
            MemBmp.Height:= Self.Height;
            XForm.eDx:= (Self.Width - Self.Width * C + Self.Height * S) / 2;
            XForm.eDy:= (Self.Height - Self.Width * S - Self.Height * C) / 2;
        end;

        SetGraphicsMode(MemBmp.Canvas.Handle, GM_ADVANCED);
        SetWorldTransform(MemBmp.Canvas.Handle, XForm);

        BitBlt(
            MemBmp.Canvas.Handle,
            0,
            0,
            MemBmp.Width,
            MemBmp.Height,
            Self.Canvas.Handle,
            0,
            0,
            SRCCOPY);

        Self.Assign(MemBmp);
    finally
        MemBmp.Free;
    end;
end;


function TBitmapHelper.WithHeight(const aHeight: Integer): TBitmap;
begin
    Result:= Self;
    Self.Height:= aHeight;
end;

function TBitmapHelper.WithWidth(const aWidth: Integer): TBitmap;
begin
    Result:= Self;
    Self.Width:= aWidth;
end;

function TBitmapHelper.Mirror(const X, Y, Width, Height: Integer; const FlipDirection: TFlipDirection): TBitmap;
var
    Dest: TRect;
begin
    Result:= Self;

    case FlipDirection of
        fdHorizontal:
            begin
                Dest.Left:= Width;
                Dest.Top:= 0;
                Dest.Right:= -Width;
                Dest.Bottom:= Height;
            end;
        fdVertical:
            begin
                Dest.Left:= 0;
                Dest.Top:= Height;
                Dest.Right:= Width;
                Dest.Bottom:= -Height;
            end;
        fdBoth:
            begin
                Dest.Left:= Width;
                Dest.Top:= Height;
                Dest.Right:= -Width;
                Dest.Bottom:= -Height;
            end;
    end;

    StretchBlt(
        Self.Canvas.Handle,
        Dest.Left,
        Dest.Top,
        Dest.Right,
        Dest.Bottom,
        Self.Canvas.Handle,
        X,
        Y,
        Width,
        Height,
        SRCCOPY);
end;

function TBitmapHelper.MirrorFrom(const bitmap: TBitmap; const X, Y, Width, Height: Integer; const FlipDirection: TFlipDirection): TBitmap;
var
    MemBmp: TBitmap;
    Dest: TRect;
begin
    Result:= Self;

    MemBmp:= TBitmapFactory.GiveMeA24bitBitmap
        .WithWidth(Width)
        .WithHeight(Height);

    try
        BitBlt(
            MemBmp.Canvas.Handle,
            0,
            0,
            Width,
            Height,
            bitmap.Canvas.Handle,
            X,
            Y,
            SRCCOPY);

        case FlipDirection of
            fdHorizontal:
                begin
                    Dest.Left:= MemBmp.Width;
                    Dest.Top:= 0;
                    Dest.Right:= -MemBmp.Width;
                    Dest.Bottom:= MemBmp.Height;
                end;
            fdVertical:
                begin
                    Dest.Left:= 0;
                    Dest.Top:= MemBmp.Height;
                    Dest.Right:= MemBmp.Width;
                    Dest.Bottom:= -MemBmp.Height;
                end;
            fdBoth:
                begin
                    Dest.Left:= MemBmp.Width;
                    Dest.Top:= MemBmp.Height;
                    Dest.Right:= -MemBmp.Width;
                    Dest.Bottom:= -MemBmp.Height;
                end;
        end;

        StretchBlt(
            MemBmp.Canvas.Handle,
            Dest.Left,
            Dest.Top,
            Dest.Right,
            Dest.Bottom,
            MemBmp.Canvas.Handle,
            0,
            0,
            MemBmp.Width,
            MemBmp.Height,
            SRCCOPY);

        Self.Assign(MemBmp);
    finally
        MemBmp.Free;
    end;
end;

function TBitmapHelper.Resize(const maxWidth, maxHeight: integer): TBitmap;
var
    MemBmp: TBitmap;
    scaledWidth, scaledHeight: integer;
begin
    Result:= Self;

    scaledWidth:= 0;
    scaledHeight:= 0;

    MemBmp:= TBitmapFactory.GiveMeA24bitBitmap;
    try
        if Self.Width > Self.Height then
        begin
            scaledHeight:= trunc(maxWidth * Self.Height / Self.Width);
            scaledWidth:= maxWidth;
        end
        else
        if Self.Height > Self.Width then
        begin
            scaledWidth:= trunc(maxHeight * Self.Width / Self.Height);
            scaledHeight:= maxHeight;
        end;

        MemBmp.Width:= maxWidth;
        MemBmp.Height:= maxHeight;
        MemBmp.Canvas.Brush.Color:= clBlack;
        MemBmp.Canvas.FillRect(Bounds(0, 0, maxWidth, maxHeight));

        MemBmp.Canvas.StretchDraw(
            Bounds(
                maxWidth div 2 - scaledWidth div 2,
                maxHeight div 2 - scaledHeight div 2,
                scaledWidth,
                scaledHeight),
            Self);

        Self.Assign(MemBmp);
    finally
        MemBmp.Free;
    end;
end;

function TBitmapHelper.RenderText(const AText: TStringList; const MaskColor: TColor = clBlack; const AMarginX: Integer = 0; const AMarginY: Integer = 0): TBitmap;
var
    Y: Integer;
    LineH, LineW: Integer;
    MemBmp: TBitmap;
begin
    Result:= Self;

    MemBmp:= TBitmapFactory.GiveMeA24bitBitmap;
    try
        MemBmp.Width:= 0;
        MemBmp.Canvas.Font.Name:= TahomaFont;
        MemBmp.Canvas.Font.Size:= 20;
        MemBmp.Canvas.Font.Color:= MaskColor;
        MemBmp.Canvas.Font.Style:= [fsBold];
        LineH:= MemBmp.Canvas.TextHeight('Wg');
        MemBmp.Height:= (AText.Count * LineH) + (AMarginY * 2);
        for Y:= 0 to AText.Count - 1 do
        begin
            LineW:= MemBmp.Canvas.TextWidth(AText.Strings[Y]) + AMarginX * 2;
            MemBmp.Width:= Max(LineW, MemBmp.Width);
        end;

        for Y:= 0 to AText.Count - 1 do
        begin
            TextOut(MemBmp.Canvas.Handle, AMarginX, AMarginY + LineH * Y, PWideChar(AText.Strings[Y]), Length(AText.Strings[Y]));
        end;

        Self.Assign(MemBmp);
    finally
        MemBmp.Free;
    end;
end;

function TBitmapHelper.RenderText(const AText: String; const MaskColor: TColor = clBlack; const AMarginX: Integer = 0; const AMarginY: Integer = 0): TBitmap;
var
    sl: TStringList;
begin
    sl:= TStringList.Create;
    try
        sl.Add(AText);
        Result:= Self.RenderText(sl, MaskColor, AMarginX, AMarginY);
    finally
        sl.Free;
    end;
end;

function TBitmapHelper.RenderMapOnText( const AText: TStringList; const bmpMap: array of TBitmap; const MaskColor: TColor = clBlack) :TBitmap;
var
    scanLine: PRGBTripleArray;
    x, y: Integer;
    MemBmp: TBitmap;
    sl: TStringList;
    s: String;
    NewBmp: TBitmap;
    bx, by: Integer;
    randomMap: Integer;
begin
    Result:= Self;

    sl:= TStringList.Create;
    try
        MemBmp:= TBitmapFactory.GiveMeA24bitBitmap.RenderText(AText);
        try
            for y:= 0 to MemBmp.Height - 1 do
            begin
                scanLine:= MemBmp.ScanLine[y];
                s:= '';
                for x:= 0 to MemBmp.Width - 1 do
                begin
                    if( Rgb(scanLine[x].rgbtRed, scanLine[x].rgbtGreen, scanLine[x].rgbtBlue) <> MaskColor) then
                    begin
                        s:= s + ' ';
                    end
                    else
                    begin
                        s:= s + '#';
                    end;
                end;
                if not s.Trim.IsEmpty then
                    sl.Add(s);
            end;
            {$IFDEF DEBUG}
            sl.SaveToFile('BitmapToAscii.txt');
            {$ENDIF}
        finally
            MemBmp.Free;
        end;

        NewBmp:= TBitmapFactory.GiveMeA24bitBitmap;
        try
            for y:= 0 to sl.Count - 1 do
            begin
                NewBmp.Width:= Max(Length(sl[y]), NewBmp.Width);
            end;

            NewBmp.Width:= NewBmp.Width * bmpMap[0].Width;
            NewBmp.Height:= sl.Count * bmpMap[0].Height;
            NewBmp.Canvas.Brush.Color := MaskColor;
            NewBmp.Canvas.FillRect(Bounds(0, 0, NewBmp.Width, NewBmp.Height));
            by:= 0;
            for y:= 0 to sl.Count - 1 do
            begin
                RandomMap:= Random(High(bmpMap));
                bx:= 0;
                for x:= 1 to Length(sl[y]) do
                begin
                    if sl[y][x] = '#' then
                        NewBmp.Canvas.Draw(bx, by, bmpMap[RandomMap]);
                    inc(bx, bmpMap[RandomMap].Width);
                end;
                inc(by, bmpMap[RandomMap].Height);
            end;

            {$IFDEF DEBUG}
            NewBmp.SaveToFile('FinalBitmap.bmp');
            {$ENDIF}

            Self.Assign(NewBmp);
        finally
            NewBmp.Free;
        end;
    finally
        sl.Free;
    end;
end;

function TBitmapHelper.RenderMapOnText(const AText: String; const bmpMap: array of TBitmap; const MaskColor: TColor = clBlack) :TBitmap;
var
    sl: TStringList;
begin
    sl:= TStringList.Create;
    try
        sl.Add(AText);
        Result:= Self.RenderMapOnText(sl, bmpMap, MaskColor);
    finally
        sl.Free;
    end;
end;


function TBitmapHelper.RenderOn(const Handle: Cardinal; const X, Y, Width, Height: Integer; const RasterOperation: Cardinal = SRCCOPY): TBitmap;
begin
    Result:= Self;

    BitBlt(
        Handle,
        X,
        Y,
        Width,
        Height,
        Self.Canvas.Handle,
        0,
        0,
        RasterOperation);
end;

function TBitmapHelper.Tint(const R, G, B :Word): TBitmap;
var
    percentage: Extended;
    h, w: Integer;
    ray: Array [0..2] of integer;
    pb: PRGBTripleArray;
    MemBmp: TBitmap;
begin
    MemBmp:= TBitmapFactory.GiveMeA24bitBitmap
        .WithWidth(Self.Width)
        .WithHeight(Self.Height);

    MemBmp.Assign(Self);

    for h:= 0 to MemBmp.Height - 1 do
    begin
        pb:= MemBmp.ScanLine[h];
        for w:= 0 to MemBmp.Width - 1 do
        begin
            //brightest byte for a percentage
            ray[0]:= pb[w].rgbtRed;
            ray[1]:= pb[w].rgbtGreen;
            ray[2]:= pb[w].rgbtBlue;

            percentage:= MaxIntValue(ray) / 255;

            pb[w].rgbtRed:= round(R * percentage);
            pb[w].rgbtGreen:= round(G * percentage);
            pb[w].rgbtBlue:= round(B * percentage);
        end;
    end;

    Result:= MemBmp;
end;

function TBitmapHelper.AsImage(const RasterOperation: Cardinal): TImage;
var
    newImg: TImage;
begin
    Result:= TImage.Create(nil);
    Result.Width:= Self.Width;
    Result.Height:= Self.Height;

    BitBlt(
        Result.Canvas.Handle,
        0,
        0,
        Result.Width,
        Result.Height,
        Self.Canvas.Handle,
        0,
        0,
        RasterOperation);
end;

function TNumberBitmap.Render(const aNumber: Integer; const PadSize: Integer = 0): TBitmap;
var
    Number: Integer;
    digit: Integer;
    MemBmp, DigitBmp: TBitmap;
    X, DigitWidth, DigitHeight: Integer;
    TotalDigits: Integer;
    DoneDigits: Integer;
begin
    Number:= aNumber;
    TotalDigits:= Ceil(log10(Number + 1));
    if Number = 0 then
    begin
        TotalDigits:= 1;
    end;

    TotalDigits:= Max(PadSize, TotalDigits);

    DigitWidth:= Self.Width div 10;
    DigitHeight:= Self.Height;

    MemBmp:= TBitmapFactory.GiveMeA24bitBitmap
                .WithWidth(DigitWidth * TotalDigits)
                .WithHeight(DigitHeight);

    DigitBmp:= TBitmapFactory.GiveMeA24bitBitmap
        .WithWidth(DigitWidth)
        .WithHeight(DigitHeight);
    try
        X:= MemBmp.Width - DigitWidth;
        DoneDigits:= 0;
        repeat
            digit:= Number mod 10;

            BitBlt(
                DigitBmp.Canvas.Handle,
                0,
                0,
                DigitWidth,
                DigitHeight,
                Self.Canvas.Handle,
                DigitWidth * digit,
                0,
                SRCCOPY);

            MemBmp.Canvas.Draw(X, 0, DigitBmp);

            dec(X, DigitWidth);

            Number:= Number div 10;
            inc(DoneDigits);
        until Number = 0;

        if DoneDigits < TotalDigits then
        begin
            BitBlt(
                DigitBmp.Canvas.Handle,
                0,
                0,
                DigitWidth,
                DigitHeight,
                Self.Canvas.Handle,
                0,
                0,
                SRCCOPY);
            while DoneDigits < TotalDigits do
            begin
                MemBmp.Canvas.Draw(X, 0, DigitBmp);
                dec(X, DigitWidth);

                inc(DoneDigits);
            end;
        end;
    finally
        DigitBmp.Free;
    end;

    Result:= MemBmp;
end;

function TBitmapHelper.AutoCrop(iBleeding: Integer; BackColor: TColor): TBitmap;
var
    Row: PRGBTripleArray;
    MyTop, MyBottom, MyLeft, i, j, MyRight: Integer;
begin
    MyTop:= Self.Height;
    MyLeft:= Self.Width;
    MyBottom:= 0;
    MyRight:= 0;

    { Find Top }
    for j:= 0 to Self.Height - 1 do
    begin
        if j > MyTop then
            Break;
        Row:= PRGBTripleArray(Self.Scanline[j]);
        for i:= Self.Width - 1 downto 0 do
            if ((Row[i].rgbtRed   <> GetRvalue(BackColor)) or
                (Row[i].rgbtGreen <> GetGvalue(BackColor)) or
                (Row[i].rgbtBlue  <> GetBvalue(BackColor))) then
            begin
                MyTop:= j;
                Break;
            end;
    end;
    if MyTop = Self.Height then{ Empty Bitmap }
        MyTop:= 0;

    { Find Bottom }
    for j:= Self.Height - 1 downto MyTop do
    begin
        if (j + 1) < MyBottom then
            Break;
        Row := PRGBTripleArray(Self.Scanline[j]);
        for i:= Self.Width - 1 downto 0  do
            if ((Row[i].rgbtRed   <> GetRvalue(BackColor)) or
                (Row[i].rgbtGreen <> GetGvalue(BackColor)) or
                (Row[i].rgbtBlue  <> GetBvalue(BackColor))) then
            begin
                MyBottom:= j + 1;
                Break;
            end;
    end;

    { Find Left }
    for j:= MyTop to MyBottom - 1 do
    begin
        Row := PRGBTripleArray(Self.Scanline[j]);
        for i:= 0 to MyLeft - 1 do
            if ((Row[i].rgbtRed   <> GetRvalue(BackColor)) or
                (Row[i].rgbtGreen <> GetGvalue(BackColor)) or
                (Row[i].rgbtBlue  <> GetBvalue(BackColor))) then
            begin
                MyLeft:= i;
                Break;
            end;
    end;
    if MyLeft = Self.Width then { Empty Bitmap }
        MyLeft:= 0;

    { Find Right }
    for j:= MyTop to MyBottom - 1 do
    begin
        Row := PRGBTripleArray(Self.Scanline[j]);
        for i:= Self.Width - 1 downto MyRight do
            if ((Row[i].rgbtRed   <> GetRvalue(BackColor)) or
                (Row[i].rgbtGreen <> GetGvalue(BackColor)) or
                (Row[i].rgbtBlue  <> GetBvalue(BackColor))) then
            begin
                MyRight:= i + 1;
                Break;
            end;
    end;
    if (MyRight = 0) or (MyBottom = 0) then { Empty Bitmap }
        iBleeding:= 0;

    Result:= TBitmapFactory.GiveMeA24bitBitmap
        .WithWidth(MyRight - MyLeft + (iBleeding * 2))
        .WithHeight(MyBottom - MyTop + (iBleeding * 2));

    Result.Canvas.Brush.Color := BackColor;
    Result.Canvas.FillRect(Rect(0, 0, Result.Width, Result.Height));

    BitBlt(
        Result.Canvas.Handle,
        -MyLeft + iBleeding,
        -MyTop + iBleeding,
        MyLeft + MyRight,
        MyTop + MyBottom,
        Self.Canvas.Handle, 0, 0, SRCCOPY);
end;

end.
