unit ConstsAndTypes;

interface

uses Vcl.Graphics;

type
    TFlipDirection = (fdNone, fdHorizontal, fdVertical, fdBoth);

    TPowerup = (puNone, puFlipHorizontal, puFlipVertical, puSpeedup, puSlowdown, puTimeFlies, puTimeFull);

    aShape = Array[1..4,1..4] of Boolean;

    Tetromino = Array[1..4] of aShape;

    TetrominoRec = record
        P: TBitmap;
        Def: Tetromino;
    end;

    XYRec = record
        X,Y :ShortInt;
        ShapeNum, ShapeRot: ShortInt;
    end;

const
    TetrominoesCount = 7;
    PieceLong = 20;
    PieceXTot = 10;
    PieceYTot = 20;
    InitialDropSpeed = 200;
    TimerInterval = 1000 div 30;
    RenderInterval = 1000 div 60;
    DefaultCountdown = 30;
    DefaultLineEventFrames = 10;

    LinesForPowerUp = 1;

    ScoreFont = 'Consolas';
    TahomaFont = 'Tahoma';

    GAME_TITLE = 'TWINTRIS';
    AUTHOR_MESSAGE = '(c) 2017 Nikos Kiokpasoglou - 40hrs challenge';
    GAMEOVER_MESSAGE = 'GAME OVER';
    INSTRUCTIONS_MESSAGE = 'Press F1 for single player or F2 for dual player battle!';

    MusicResource = 'DAMUZAK';

    XX = True;
    __ = False;

    TetrominoStraight: Tetromino =
    ((
     (__,__,__,__),
     (__,__,__,__),
     (XX,XX,XX,XX),
     (__,__,__,__)),

    ((__,XX,__,__),
     (__,XX,__,__),
     (__,XX,__,__),
     (__,XX,__,__)),

    ((__,__,__,__),
     (XX,XX,XX,XX),
     (__,__,__,__),
     (__,__,__,__)),

    ((__,XX,__,__),
     (__,XX,__,__),
     (__,XX,__,__),
     (__,XX,__,__)));

    TetrominoT: Tetromino =
    ((
     (__,XX,__,__),
     (XX,XX,XX,__),
     (__,__,__,__),
     (__,__,__,__)),

    ((__,XX,__,__),
     (XX,XX,__,__),
     (__,XX,__,__),
     (__,__,__,__)),

    ((__,__,__,__),
     (XX,XX,XX,__),
     (__,XX,__,__),
     (__,__,__,__)),

    ((__,XX,__,__),
     (__,XX,XX,__),
     (__,XX,__,__),
     (__,__,__,__)));

    TetrominoCube: Tetromino =
    ((
     (__,__,__,__),
     (__,XX,XX,__),
     (__,XX,XX,__),
     (__,__,__,__)),

    ((__,__,__,__),
     (__,XX,XX,__),
     (__,XX,XX,__),
     (__,__,__,__)),

    ((__,__,__,__),
     (__,XX,XX,__),
     (__,XX,XX,__),
     (__,__,__,__)),

    ((__,__,__,__),
     (__,XX,XX,__),
     (__,XX,XX,__),
     (__,__,__,__)));

    TetrominoZ: Tetromino =
    ((
     (__,__,__,__),
     (__,__,XX,__),
     (__,XX,XX,__),
     (__,XX,__,__)),

    ((__,__,__,__),
     (__,XX,XX,__),
     (__,__,XX,XX),
     (__,__,__,__)),

    ((__,__,__,__),
     (__,__,XX,__),
     (__,XX,XX,__),
     (__,XX,__,__)),

    ((__,__,__,__),
     (__,XX,XX,__),
     (__,__,XX,XX),
     (__,__,__,__)));

    TetrominoS: Tetromino =
    ((
     (__,__,__,__),
     (__,XX,__,__),
     (__,XX,XX,__),
     (__,__,XX,__)),

    ((__,__,__,__),
     (__,XX,XX,__),
     (XX,XX,__,__),
     (__,__,__,__)),

    ((__,__,__,__),
     (__,XX,__,__),
     (__,XX,XX,__),
     (__,__,XX,__)),

    ((__,__,__,__),
     (__,XX,XX,__),
     (XX,XX,__,__),
     (__,__,__,__)));

    TetrominoL: Tetromino =
    ((
     (__,__,__,__),
     (__,XX,XX,__),
     (__,__,XX,__),
     (__,__,XX,__)),

    ((__,__,__,__),
     (__,XX,XX,XX),
     (__,XX,__,__),
     (__,__,__,__)),

    ((__,__,__,__),
     (__,XX,__,__),
     (__,XX,__,__),
     (__,XX,XX,__)),

    ((__,__,__,__),
     (__,__,__,XX),
     (__,XX,XX,XX),
     (__,__,__,__)));

    TetrominoJ: Tetromino =
    ((
     (__,__,__,__),
     (__,XX,XX,__),
     (__,XX,__,__),
     (__,XX,__,__)),

    ((__,__,__,__),
     (__,XX,XX,XX),
     (__,__,__,XX),
     (__,__,__,__)),

    ((__,__,__,__),
     (__,__,XX,__),
     (__,__,XX,__),
     (__,XX,XX,__)),

    ((__,__,__,__),
     (__,XX,__,__),
     (__,XX,XX,XX),
     (__,__,__,__)));

implementation

end.
