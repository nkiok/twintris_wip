unit SoundHandler;

interface

procedure PlaySoundFromResource(const AResName: string; Loop: Boolean = False);
procedure StopSound;

implementation

uses WinApi.Windows, System.SysUtils, MMSystem;

procedure StopSound;
begin
    sndPlaySound(nil, SND_NODEFAULT); // nil = stop currently playing
end;

procedure PlaySoundFromResource(const AResName: string; Loop: Boolean = False);
var
    HResource: TResourceHandle;
    HResData: THandle;
    PWav: Pointer;
begin
    HResource:= FindResource(HInstance, PWideChar(AResName), RT_RCDATA);
    if HResource <> 0 then
    begin
        HResData:= LoadResource(HInstance, HResource);
        if HResData <> 0 then
        begin
            PWav:= LockResource(HResData);
            if Assigned(PWav) then
            begin
                StopSound;
                if Loop then
                    sndPlaySound(PWav, SND_ASYNC or SND_MEMORY or SND_LOOP)
                else
                    sndPlaySound(PWav, SND_ASYNC or SND_MEMORY);
            end;
        end;
    end
    else
        RaiseLastOSError;
end;

end.
