unit Tetris;

interface

uses
  WinApi.Windows, System.Classes, System.SysUtils, System.Generics.Collections,
  Forms, Vcl.ExtCtrls, Vcl.Graphics, Vcl.Controls, Vcl.Menus, System.Actions,
  Vcl.ActnList, Vcl.StdActns,
  ConstsAndTypes, PlayArea, Bitmaps;

type
  TForm1 = class(TForm)
    GameLoopTimer: TTimer;
    MainMenu1: TMainMenu;
    N1: TMenuItem;
    Exit1: TMenuItem;
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    Image4: TImage;
    Image5: TImage;
    Image6: TImage;
    Image7: TImage;
    AreaP1: TImage;
    ScoreP1: TImage;
    ScoreP2: TImage;
    AreaP2: TImage;
    mSingle: TMenuItem;
    ActionList1: TActionList;
    aSingleMode: TAction;
    aDualMode: TAction;
    mDual: TMenuItem;
    FileExit1: TFileExit;
    ScrollTimer: TTimer;
    CountdownTimer: TTimer;
    RenderTimer: TTimer;
    PowerupFlipHorizontal: TImage;
    PowerupSpeedup: TImage;
    PowerupSlowdown: TImage;
    PowerupFlipVertical: TImage;
    Image8: TImage;
    Image9: TImage;
    Image10: TImage;
    Image11: TImage;
    Image12: TImage;
    PowerupTimeFlies: TImage;
    PowerupTimeFull: TImage;
    imgClientArea: TImage;

    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure GameLoopTimerTimer(Sender: TObject);
    procedure ScrollTimerTimer(Sender: TObject);
    procedure CountdownTimerTimer(Sender: TObject);
    procedure RenderTimerTimer(Sender: TObject);
    procedure aSingleModeExecute(Sender: TObject);
    procedure aDualModeExecute(Sender: TObject);
    procedure aMenuItemsUpdate(Sender: TObject);
    procedure Exit1Click(Sender: TObject);

  private
    ScoreBackground: TBitmap;

    PlayAreaBgColor: Integer;

    BlendedBitmap: TBitmap;

    Combos: TBitmap;

    ScoreDigits: TNumberBitmap;

    CountdownDigits: TNumberBitmap;

    LineDigits: TNumberBitmap;

    TopPanel: TBitmap;

    BottomPanel: TBitmap;

    BackgroundTile: TBitmap;


    ScrollOffsetX, ScrollOffsetY: Integer;

    ScrollMessage: String;


    SinPoints: Array of integer;

    SinusScrollOffsetX: Integer;

    SinusScrollMessage: String;

    RotatingLogo: TBitmap;

    ScrollDeg: Single;


    RotatingMessage: String;

    RotationDirection: Integer;


    PlayAreas: TList<TPlayArea>;


    procedure Tile(const Source: TBitmap; const Dest: TBitmap);

    procedure DrawScoreBG(const PlayArea: TPlayArea);
    procedure DrawPowerup(const PlayArea: TPlayArea);
    procedure DrawBlend;
    procedure DrawPiece(const PlayArea: TPlayArea; const X, Y, Piece: Word);
    procedure DrawShape(const PlayArea: TPlayArea; const ShapeNum, ShapeRotation, X, Y: Word);
    function CreateShape(const PlayArea: TPlayArea; const ShapeNum, ShapeRotation: Integer): TBitmap;
    procedure DrawScoreAndNextPiece(const PlayArea: TPlayArea);
    procedure DrawComboInPlayArea(const PlayArea: TPlayArea);
    procedure DrawCountdownAndPowerup(const PlayArea: TPlayArea);
    procedure DrawTetris(const PlayArea: TPlayArea);
    procedure CheckLineEvent(const PlayArea: TPlayArea);
    procedure NewShape(const PlayArea: TPlayArea);
    procedure ClearLocation(const PlayArea: TPlayArea; const X, Y: Word);
    procedure LeftPiece(const PlayArea: TPlayArea);
    procedure RightPiece(const PlayArea: TPlayArea);
    procedure DownPiece(const PlayArea: TPlayArea);
    procedure RotateIt(const PlayArea: TPlayArea);
    procedure RestartGame;
    procedure RedrawPieces(const PlayArea: TPlayArea);
    procedure SetArrowsForKeyboardInput(const PlayArea: TPlayArea);
    procedure SetWASDForKeyboardInput(const PlayArea: TPlayArea);
    procedure CheckInput;
    procedure Clear2Lines(const PlayArea: TPlayArea);
    function AddPlayArea(const DrawPlayArea, DrawScoreArea: TImage;
                         const Who: String;
                         const Images: TList<TImage>): TPlayArea;
    procedure BackBufferTextOut(const DrawArea: TImage; const S: String;
                                const FontName: String; const FontSize: Integer; const FontColor: TColor;
                                const OffsetX, OffsetY: Integer;
                                const Transparent: Boolean = True);
    function AllGameOver: Boolean;
    procedure ClearPlayAreas;
    function SetupSingle: TPlayArea;
    function SetupDual: TPlayArea;
    procedure InitScroller(const aMessage: String);
    procedure InitSinusScroller(const aMessage: String);
    procedure InitRotatingMessage(const aMessage: String);
    procedure Scroll;
    function GetPowerupBitmap(const PlayArea: TPlayArea): TBitmap;
    procedure RemovePowerups;
  end;

var
    Form1: TForm1;

implementation

{$R *.dfm}

uses Math, SoundHandler, PlayerInputProvider, InitializableList;

procedure TForm1.FormCreate(Sender: TObject);
begin
    PlayAreaBgColor:= RGB(110, 110, 110);
    RenderTimer.Enabled:= False;
    RenderTimer.Interval:= RenderInterval;
    GameLoopTimer.Interval:= TimerInterval;
    GameLoopTimer.Enabled:= False;
    ScrollTimer.Enabled:= False;
    CountdownTimer.Enabled:= False;

    BackgroundTile:= TBitmapFactory.GiveMeA24bitBitmapFromResource('BACKGROUNDTILE');

    ScoreBackground:= TBitmapFactory.GiveMeA24bitBitmap
        .WithWidth(ScoreP1.Width)
        .WithHeight(ScoreP1.Height);
    Tile(BackgroundTile, ScoreBackground);

    ScoreP1.Picture.Bitmap.Assign(ScoreBackground.Tint(50, 70, 90));
    ScoreP2.Picture.Bitmap.Assign(ScoreBackground.Tint(90, 50, 70));

    Combos:= TBitmapFactory.GiveMeA32bitBitmapFromResource('COMBOS');

    ScoreDigits:= TNumberBitmap.Create;
    ScoreDigits.LoadFromResourceName(HInstance, 'DIGITS');

    TopPanel:= TBitmapFactory.GiveMeA24bitBitmapFromResource('PANEL2');
    BottomPanel:= TBitmapFactory.GiveMeA24bitBitmapFromResource('PANEL1');

    CountdownDigits:= TNumberBitmap.Create;
    CountdownDigits.Assign(ScoreDigits.Tint(255, 0, 0));

    LineDigits:= TNumberBitmap.Create;
    LineDigits.Assign(ScoreDigits.Tint(200, 200, 100));

    Randomize;

    BlendedBitmap:= TBitmapFactory.GiveMeA24bitBitmap
        .WithWidth(ClientWidth)
        .WithHeight(ClientHeight);
    BlendedBitmap.Canvas.Font:= Self.Canvas.Font;

    RotatingLogo:= TBitmapFactory.GiveMeA24bitBitmap
        .RenderMapOnText(
            GAME_TITLE,
            [Image1.Picture.Bitmap,
             Image2.Picture.Bitmap,
             Image3.Picture.Bitmap,
             Image4.Picture.Bitmap,
             Image5.Picture.Bitmap,
             Image6.Picture.Bitmap,
             Image7.Picture.Bitmap,
             Image8.Picture.Bitmap,
             Image9.Picture.Bitmap,
             Image12.Picture.Bitmap])
        .Resize(ClientWidth, ClientHeight);

    PlayAreas:= TList<TPlayArea>.Create;

    PlaySoundFromResource(MusicResource, True);
    InitScroller(INSTRUCTIONS_MESSAGE);
    InitSinusScroller(AUTHOR_MESSAGE);
    InitRotatingMessage(GAME_TITLE);
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
    ScoreBackground.Free;
    BackgroundTile.Free;
    Combos.Free;
    ScoreDigits.Free;
    CountdownDigits.Free;
    TopPanel.Free;
    BottomPanel.Free;
    LineDigits.Free;
    PlayAreas.Free;
    SinPoints:= nil;
end;

procedure TForm1.FormResize(Sender: TObject);
begin
    BlendedBitmap.Height:= ClientHeight;
    BlendedBitmap.Width:= ClientWidth;
    Tile(BackgroundTile, BlendedBitmap);
    DrawBlend;
end;

procedure TForm1.FormPaint(Sender: TObject);
var
    flipped: TBitmap;
    PlayArea: TPlayArea;
begin
    BlendedBitmap.RenderOn(
        Self.Canvas.Handle,
        0,
        0,
        ClientWidth,
        ClientHeight);

    for PlayArea in PlayAreas do
    begin
        if not PlayArea.GameOver then
        begin
            if PlayArea.FlipDirection <> fdNone then
            begin
                flipped:= TBitmapFactory.GiveMeA24bitBitmap
                            .MirrorFrom(
                                BlendedBitmap,
                                PlayArea.DrawPlayArea.Left,
                                PlayArea.DrawPlayArea.Top,
                                PlayArea.DrawPlayArea.Width,
                                PlayArea.DrawPlayArea.Height,
                                PlayArea.FlipDirection)
                            .RenderOn(
                                Self.Canvas.Handle,
                                PlayArea.DrawPlayArea.Left,
                                PlayArea.DrawPlayArea.Top,
                                PlayArea.DrawPlayArea.Width,
                                PlayArea.DrawPlayArea.Height,
                                SRCCOPY);
                flipped.Free;
            end;
        end;
    end;
end;

procedure TForm1.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    CheckInput;
    Key:= 0;
end;

procedure TForm1.FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
var
    PlayArea: TPlayArea;
begin
    for PlayArea in PlayAreas do
    begin
        if not PlayArea.GameOver then
        begin
            if not PlayArea.LineEvent then
            begin
                if PlayArea.PlayerInput.IsHardDrop(Key) then
                begin
                    while not PlayArea.ReachedBottom do
                    begin
                        DownPiece(PlayArea);
                        CheckLineEvent(PlayArea);
                    end;
                    NewShape(PlayArea);
                end else
                if PlayArea.PlayerInput.IsRotate(Key) then
                begin
                    RotateIt(PlayArea);
                end;

                if PlayArea.PlayerInput.IsPowerup(Key) then
                begin
                    PlayArea.ActivatePowerup;
                    DrawScoreAndNextPiece(PlayArea);
                end;
            end;
        end;
    end;

    Key:= 0;

end;

procedure TForm1.GameLoopTimerTimer(Sender: TObject);
var
    PlayArea: TPlayArea;
begin
    if AllGameOver then
    begin
        RenderTimer.Enabled:= False;
        GameLoopTimer.Enabled:= False;
        CountdownTimer.Enabled:= True;
        case PlayAreas.Count of
            1 : InitSinusScroller('You can do better...');
            2 : if abs(PlayAreas[0].Score - PlayAreas[1].Score) > 0 then
                begin
                    if PlayAreas[0].Score > PlayAreas[1].Score then
                        InitSinusScroller('Player #1 Winner Winner Chicken Dinner!')
                    else
                        InitSinusScroller('Player #2 Winner Winner Chicken Dinner!');
                    PlaySoundFromResource(MusicResource);
                end
                else
                begin
                    InitSinusScroller('You both suck!');
                end;
        end;
        InitScroller(INSTRUCTIONS_MESSAGE);
        InitRotatingMessage(GAME_TITLE);
        exit;
    end;

    for PlayArea in PlayAreas do
    begin
        DrawTetris(PlayArea);
    end;
end;

procedure TForm1.ScrollTimerTimer(Sender: TObject);
begin
    Scroll;
end;

procedure TForm1.CountdownTimerTimer(Sender: TObject);
var
    PlayArea: TPlayArea;
begin
    for PlayArea in PlayAreas do
    begin
        if not PlayArea.GameOver then
        begin
            PlayArea.UpdateCountdown;
            DrawCountdownAndPowerup(PlayArea);
        end;
    end;
end;

procedure TForm1.RenderTimerTimer(Sender: TObject);
begin
    FormPaint(nil);
end;

procedure TForm1.aSingleModeExecute(Sender: TObject);
begin
    SetupSingle;
    RestartGame;
end;

procedure TForm1.aDualModeExecute(Sender: TObject);
begin
    SetupDual;
    RestartGame;
end;

procedure TForm1.aMenuItemsUpdate(Sender: TObject);
begin
    (Sender as TAction).Enabled:= (not GameLoopTimer.Enabled) or AllGameOver;
end;

procedure TForm1.Exit1Click(Sender: TObject);
begin
    Close;
end;

procedure TForm1.Tile(const Source: TBitmap; const Dest: TBitmap);
var
    x, y: integer;
    h, w: integer;
begin
    h:= Source.Height;
    w:= Source.Width;
    for x:= 0 to 7 do
        for y:= 0 to 7 do
            Dest.Canvas.Draw(x * w, y * h, Source);
end;

procedure TForm1.DrawScoreBG(const PlayArea: TPlayArea);
begin
    BlendedBitmap.Canvas.Draw(
        PlayArea.DrawScoreArea.Left,
        PlayArea.DrawScoreArea.Top,
        PlayArea.DrawScoreArea.Picture.Bitmap);
end;

procedure TForm1.DrawPowerup(const PlayArea: TPlayArea);
begin
    if PlayArea.ActivePowerup = puNone then
        Exit;

    GetPowerupBitmap(PlayArea)
        .RenderTransparentOn(
            BlendedBitmap.Canvas,
            PlayArea.DrawPlayArea.Left + PlayArea.DrawPlayArea.Width + 65,
            PlayArea.DrawPlayArea.Top + 30,
            Rgb(22, 22, 22));
end;

procedure TForm1.DrawBlend;
var
    PlayArea: TPlayArea;
begin
    if GameLoopTimer.Enabled then
    begin
        for PlayArea in PlayAreas do
        begin
            if not PlayArea.GameOver then
            begin
                DrawScoreAndNextPiece(PlayArea);
            end;
        end;
    end;
end;

procedure TForm1.DrawPiece(const PlayArea: TPlayArea; const X, Y, Piece: Word);
begin
    if Piece = 0 then
        Exit;

    BlendedBitmap.Canvas.Draw(
        PlayArea.DrawPlayArea.Left + (X - 1) * PieceLong,
        PlayArea.DrawPlayArea.Top +  (Y - 1) * PieceLong,
        PlayArea.Tetrominoes[Piece].P);
end;

procedure TForm1.DrawShape(const PlayArea: TPlayArea; const ShapeNum, ShapeRotation, X, Y: Word); {X,Y = 1..20 # Shape Rotation = 1..4}
var
    X1, Y1: Byte;
begin
    for X1:= 1 to 4 do
        for Y1:= 1 to 4 do
            if PlayArea.ShapeFilledAt(ShapeNum, ShapeRotation, y1, x1) then
            begin
                if Y + Y1 > 1 then
                    BlendedBitmap.Canvas.Draw(
                        PlayArea.DrawPlayArea.Left + (X1 + X - 2) * PieceLong,
                        PlayArea.DrawPlayArea.Top + (Y1 + Y - 2) * PieceLong,
                        PlayArea.Tetrominoes[ShapeNum].P);
            end;
end;

function TForm1.CreateShape(const PlayArea: TPlayArea; const ShapeNum, ShapeRotation: Integer): TBitmap;
var
    X, Y: Byte;
    MemBmp: TBitmap;
begin
    MemBmp:= TBitmapFactory.GiveMeA24bitBitmap
        .WithWidth(4 * PieceLong)
        .WithHeight(4 * PieceLong);
    try
        for X:= 1 to 4 do
            for Y:= 1 to 4 do
                if PlayArea.ShapeFilledAt(ShapeNum, ShapeRotation, Y, X) then
                begin
                    MemBmp.Canvas.Draw(
                        (X - 1) * PieceLong,
                        (Y - 1) * PieceLong,
                        PlayArea.Tetrominoes[ShapeNum].P);
                end;

        Result:= MemBmp.AutoCrop(0, clWhite);
    finally
        MemBmp.Free;
    end;
end;

procedure TForm1.DrawScoreAndNextPiece(const PlayArea: TPlayArea);
var
    S: String;
    TmpRot, TmpShape: Word;
    RenderedBmp: TBitmap;
begin
    DrawScoreBG(PlayArea);

    DrawCountdownAndPowerup(PlayArea);

    BackBufferTextOut(
        PlayArea.DrawScoreArea,
        PlayArea.Who,
        ScoreFont,
        16,
        clYellow,
        2,
        0);

    RenderedBmp:= ScoreDigits.Render(PlayArea.Score, 6);
    try
        RenderedBmp.RenderTransparentOn(
                BlendedBitmap.Canvas,
                PlayArea.DrawScoreArea.Left + PlayArea.DrawScoreArea.Width - RenderedBmp.Width,
                PlayArea.DrawScoreArea.Top + 30,
                clBlack);
    finally
        RenderedBmp.Free;
    end;

    RenderedBmp:= LineDigits.Render(PlayArea.Lines);
    try
        RenderedBmp.RenderTransparentOn(
                BlendedBitmap.Canvas,
                PlayArea.DrawScoreArea.Left + PlayArea.DrawScoreArea.Width - RenderedBmp.Width,
                PlayArea.DrawScoreArea.Top + 60,
                clBlack);
    finally
        RenderedBmp.Free;
    end;

    // Draw the next shape
    TmpShape:= PlayArea.CurrentTetromino.ShapeNum;
    PlayArea.CurrentTetromino.ShapeNum:= PlayArea.NextTetromino;
    TmpRot:= PlayArea.CurrentTetromino.ShapeRot;
    PlayArea.CurrentTetromino.ShapeRot:= 1;

    BottomPanel.RenderTransparentOn(
        BlendedBitmap.Canvas,
        PlayArea.DrawPlayArea.Left + PlayArea.DrawPlayArea.Width + 1,
        PlayArea.DrawPlayArea.Top + PlayArea.DrawPlayArea.Height - BottomPanel.Height,
        clBlack);

    RenderedBmp:= CreateShape(
            PlayArea,
            PlayArea.NextTetromino,
            1);

    RenderedBmp
        .RenderTransparentOn(
            BlendedBitmap.Canvas,
            (PlayArea.DrawPlayArea.Left + PlayArea.DrawPlayArea.Width + BottomPanel.Width div 2) - RenderedBmp.Width div 2,
            (PlayArea.DrawPlayArea.Top + PlayArea.DrawPlayArea.Height - BottomPanel.Height div 2) - RenderedBmp.Height div 2,
            clWhite);

    RenderedBmp.Free;

    PlayArea.CurrentTetromino.ShapeNum:= TmpShape;
    PlayArea.CurrentTetromino.ShapeRot:= TmpRot;
end;

procedure TForm1.DrawComboInPlayArea(const PlayArea: TPlayArea);
var
    combo: TBitmap;
begin
    if PlayArea.ComboMultiplier < 1 then
        Exit;

    combo:= TBitmapFactory
        .GiveMeA32bitBitmap
        .WithWidth(Combos.Width div 10)
        .WithHeight(Combos.Height);

    try
        BitBlt(
            combo.Canvas.Handle,
            0,
            0,
            combo.Width,
            combo.Height,
            Combos.Canvas.Handle,
            combo.Width * (PlayArea.ComboMultiplier - 1),
            0,
            SRCCOPY);

        combo.Resize(
                combo.Width + PlayArea.LineEventFrames * 4,
                combo.Height + PlayArea.LineEventFrames * 4)
            .RenderTransparentOn(
                BlendedBitmap.Canvas,
                PlayArea.DrawPlayArea.Left + ((PlayArea.DrawPlayArea.Width - combo.Width) div 2),
                PlayArea.DrawPlayArea.Top + ((PlayArea.DrawPlayArea.Height - combo.Height) div 2),
                clBlack);
    finally
        combo.Free;
    end;
end;

procedure TForm1.DrawCountdownAndPowerup(const PlayArea: TPlayArea);
var
    RenderedBmp: TBitmap;
begin
    TopPanel.RenderTransparentOn(
        BlendedBitmap.Canvas,
        PlayArea.DrawPlayArea.Left + PlayArea.DrawPlayArea.Width + 1,
        PlayArea.DrawPlayArea.Top - 50,
        clBlack);

    RenderedBmp:= CountdownDigits.Render(PlayArea.CountDown, 2);
    try
        RenderedBmp.RenderTransparentOn(
                BlendedBitmap.Canvas,
                PlayArea.DrawPlayArea.Left + PlayArea.DrawPlayArea.Width + 55,
                PlayArea.DrawPlayArea.Top,
                clBlack);
    finally
        RenderedBmp.Free;
    end;

    DrawPowerup(PlayArea);
end;

procedure TForm1.DrawTetris(const PlayArea: TPlayArea);
begin
    if PlayArea.GameOver then
        Exit;

    if (PlayArea.TetCount > PlayArea.TetToCount) and (not PlayArea.LineEvent) then
    begin
        PlayArea.TetCount:= 0;
        if PlayArea.ReachedBottom then
        begin
            if PlayArea.CurrentTetromino.Y <= 1 then
            begin
                PlayArea.GameOver:= True;
                if Assigned(PlayArea.Opponent) then
                begin
                    PlayArea.Opponent.Opponent:= nil;
                end;
                RemovePowerups;
                PlaySoundFromResource('GameOver');

                BackBufferTextOut(
                    PlayArea.DrawPlayArea,
                    GAMEOVER_MESSAGE,
                    TahomaFont,
                    25,
                    clRed,
                    0,
                    PieceYTot * PieceLong div 2,
                    false);
            end
            else
            begin
                NewShape(PlayArea);
            end;
        end
        else
        begin
            DownPiece(PlayArea);
        end;
    end
    else
        Inc(PlayArea.TetCount);

    CheckLineEvent(PlayArea);
end;

procedure TForm1.CheckLineEvent(const PlayArea: TPlayArea);
var
    X, X1, R: Word;
begin
    if (PlayArea.LineEvent) then
    begin
        R:= PlayArea.PickTetromino;

        for X:= 1 to PieceYTot do
            if PlayArea.LineEventLines[X] then
                for X1:= 1 to PieceXTot do
                    DrawPiece(PlayArea, X1, X, R);

        DrawComboInPlayArea(PlayArea);
        Inc(PlayArea.LineEventFrames);

        if PlayArea.LineEventFrames > DefaultLineEventFrames then
        begin
            PlayArea.LineEvent:= False;
            PlayArea.LineEventFrames:= 0;

            // Kill full lines
            for X:= 1 to PieceYTot do
                if PlayArea.LineEventLines[X] then
                begin
                    for X1:= 1 to PieceXTot do
                        PlayArea.Piece[X1, X]:= 0;
                    for X1:= X downto 2 do
                        for R:=1 to PieceXTot do
                            PlayArea.Piece[R, X1]:= PlayArea.Piece[R, X1 - 1];
                end;

            // Redraw
            for X:= 1 to PieceXTot do
                for X1:= 1 to PieceYTot do
                    if PlayArea.Piece[X, X1] = 0 then
                        ClearLocation(PlayArea, X, X1) else
                            DrawPiece(PlayArea, X, X1, PlayArea.Piece[X, X1]);

            // Start from next pending Shape
            PlayArea.CurrentTetromino.ShapeNum:= PlayArea.NextTetromino;
            PlayArea.CurrentTetromino.ShapeRot:= 1;
            PlayArea.CurrentTetromino.X:= PieceXTot div 2 - 1;
            PlayArea.CurrentTetromino.Y:= 2 - PlayArea.PieceUpSide;

            DrawShape(
                PlayArea,
                PlayArea.CurrentTetromino.ShapeNum,
                PlayArea.CurrentTetromino.ShapeRot,
                PlayArea.CurrentTetromino.X,
                PlayArea.CurrentTetromino.Y);

            PlayArea.NextTetromino:= PlayArea.PickTetromino;

            DrawScoreAndNextPiece(PlayArea);
        end;
    end;
end;

procedure TForm1.RestartGame;
var
    DRect: TRect;
    X, Y: ShortInt;
    PlayArea: TPlayArea;
begin
    PlaySoundFromResource('GameStart');
    ScrollTimer.Enabled:= False;

    Tile(BackgroundTile, BlendedBitmap);
    DrawBlend;

    RenderTimer.Enabled:= True;
    GameLoopTimer.Enabled:= True;
    CountdownTimer.Enabled:= True;
    for PlayArea in PlayAreas do
    begin
        PlayArea.Restart;
        DRect:= Rect(
            PlayArea.DrawPlayArea.Left - 1,
            PlayArea.DrawPlayArea.Top - 1,
            PlayArea.DrawPlayArea.Left + PlayArea.DrawPlayArea.Width + 1,
            PlayArea.DrawPlayArea.Top + PlayArea.DrawPlayArea.Height + 1);

        BlendedBitmap.Canvas.Brush.Color:= PlayAreaBgColor;
        BlendedBitmap.Canvas.Pen.Color:= clAqua;
        BlendedBitmap.Canvas.Rectangle(DRect);
        DrawScoreAndNextPiece(PlayArea);
    end;
end;

function TForm1.AllGameOver: Boolean;
var
    PlayArea: TPlayArea;
begin
    Result:= True;
    for PlayArea in PlayAreas do
    begin
        Result:= Result and PlayArea.GameOver;
    end;
end;

procedure TForm1.ClearLocation(const PlayArea: TPlayArea; const X, Y: Word);
var
    DRect: TRect;
begin
    if Y = 0 then
        Exit;

    DRect:= Rect(
        PlayArea.DrawPlayArea.Left + PieceLong * (X - 1),
        PlayArea.DrawPlayArea.Top + PieceLong * (Y - 1),
        PlayArea.DrawPlayArea.Left + PieceLong * (X),
        PlayArea.DrawPlayArea.Top + PieceLong * (Y));

    BlendedBitMap.Canvas.Pen.Style:= psSolid;
    BlendedBitMap.Canvas.Pen.Color:= PlayAreaBgColor;
    BlendedBitMap.Canvas.Brush.Style:= bsSolid;
    BlendedBitMap.Canvas.Brush.Color:= PlayAreaBgColor;
    BlendedBitmap.Canvas.Rectangle(DRect);
end;

procedure TForm1.BackBufferTextOut(const DrawArea: TImage; const S: String;
                              const FontName: String; const FontSize: Integer; const FontColor: TColor;
                              const OffsetX, OffsetY: Integer;
                              const Transparent: Boolean = True);
begin
    BlendedBitmap.Canvas.Font.Name:= FontName;
    BlendedBitmap.Canvas.Font.Size:= FontSize;
    BlendedBitmap.Canvas.Font.Color:= FontColor;
    BlendedBitmap.Canvas.Font.Style:= [fsBold];
    if Transparent then
        BlendedBitmap.Canvas.Brush.Style:= bsClear
    else
        BlendedBitmap.Canvas.Brush.Color:= clBlack;

    BlendedBitmap.Canvas.TextOut(
        DrawArea.Left + OffsetX,
        DrawArea.Top + OffsetY,
        S);

end;

function TForm1.AddPlayArea(const DrawPlayArea, DrawScoreArea: TImage;
                            const Who: String;
                            const Images: TList<TImage>): TPlayArea;
var
    PlayArea: TPlayArea;
    Image: TImage;
    i: Integer;
begin
    PlayArea:= TPlayArea.Create(DrawPlayArea, DrawScoreArea, Who);

    i:= 1;
    for Image in Images do
    begin
        PlayArea.AssignShape(i, Image);
        Inc(i);
    end;
    PlayAreas.Add(PlayArea);

    Result:= PlayArea;
end;

procedure TForm1.NewShape(const PlayArea: TPlayArea);
var
    X, Y: ShortInt;
begin
    // Saves the old shape
    for X:= 1 to 4 do
        for Y:= 1 to 4 do
            if PlayArea.Tetrominoes[PlayArea.CurrentTetromino.ShapeNum].Def[PlayArea.CurrentTetromino.ShapeRot, Y, X] then
                PlayArea.Piece[PlayArea.CurrentTetromino.X + X - 1, PlayArea.CurrentTetromino.Y + Y - 1]:= PlayArea.CurrentTetromino.ShapeNum;

    if PlayArea.CheckLines then
    begin
        DrawScoreAndNextPiece(PlayArea);
        Exit;
    end
    else
    begin
        PlayArea.ComboMultiplier:= 0;
    end;

    // Creates a new shape here
    PlayArea.CurrentTetromino.ShapeNum:= PlayArea.NextTetromino;
    PlayArea.CurrentTetromino.ShapeRot:= 1;
    PlayArea.CurrentTetromino.X:= PieceXTot div 2 - 1;
    PlayArea.CurrentTetromino.Y:= 2 - PlayArea.PieceUpSide;

    DrawShape(
        PlayArea,
        PlayArea.CurrentTetromino.ShapeNum,
        PlayArea.CurrentTetromino.ShapeRot,
        PlayArea.CurrentTetromino.X,
        PlayArea.CurrentTetromino.Y);

    PlayArea.NextTetromino:= PlayArea.PickTetromino;
    DrawScoreAndNextPiece(PlayArea);
end;

procedure TForm1.LeftPiece(const PlayArea: TPlayArea);
var
    B: Array[1..4] of Byte;
    B1, B2: Byte;
begin
    ZeroMemory(@B, Sizeof(B));

    for B1:= 4 downto 1 do
        for B2:= 4 downto 1 do
            if (PlayArea.CurrentShapeFilledAt(B1, B2)) and (B[B1] = 0) then
                B[B1]:= B2;

    for B1:= 1 to 4 do
        if B[B1] <> 0 then
            ClearLocation(PlayArea, PlayArea.CurrentTetromino.X + B[B1] - 1, PlayArea.CurrentTetromino.Y + B1 - 1);

    Dec(PlayArea.CurrentTetromino.X);

    DrawShape(
        PlayArea,
        PlayArea.CurrentTetromino.ShapeNum,
        PlayArea.CurrentTetromino.ShapeRot,
        PlayArea.CurrentTetromino.X,
        PlayArea.CurrentTetromino.Y);
end;

procedure TForm1.RightPiece(const PlayArea: TPlayArea);
var
    B: Array[1..4] of Byte;
    B1, B2: Byte;
begin
    ZeroMemory(@B, Sizeof(B));

    for B1:= 1 to 4 do
        for B2:= 1 to 4 do
            if (PlayArea.CurrentShapeFilledAt(B1, B2)) and (B[B1] = 0) then
                B[B1]:= B2;

    for B1:= 1 to 4 do
        if B[B1] <> 0 then
            ClearLocation(PlayArea, PlayArea.CurrentTetromino.X + B[B1] - 1, PlayArea.CurrentTetromino.Y + B1 - 1);

    Inc(PlayArea.CurrentTetromino.X);

    DrawShape(
        PlayArea,
        PlayArea.CurrentTetromino.ShapeNum,
        PlayArea.CurrentTetromino.ShapeRot,
        PlayArea.CurrentTetromino.X,
        PlayArea.CurrentTetromino.Y);
end;

procedure TForm1.DownPiece(const PlayArea: TPlayArea);
var
    B: Array[1..4] of Byte;
    B1, B2: Byte;
begin
    ZeroMemory(@B, Sizeof(B));

    for B1:= 1 to 4 do
        for B2:= 1 to 4 do
            if PlayArea.CurrentShapeFilledAt(B2, B1) then
                if (B[B1] = 0) then
                    B[B1]:= B2;

    for B1:= 1 to 4 do
        if B[B1] <> 0 then
            ClearLocation(PlayArea, PlayArea.CurrentTetromino.X + B1 - 1, PlayArea.CurrentTetromino.Y + B[B1] - 1);

    Inc(PlayArea.CurrentTetromino.Y);

    DrawShape(
        PlayArea,
        PlayArea.CurrentTetromino.ShapeNum,
        PlayArea.CurrentTetromino.ShapeRot,
        PlayArea.CurrentTetromino.X,
        PlayArea.CurrentTetromino.Y);
end;

procedure TForm1.RotateIt(const PlayArea: TPlayArea);
var
    NewRot, OldRot: ShortInt;
    B1, B2: Byte;
begin
    NewRot:= PlayArea.CurrentTetromino.ShapeRot;
    Inc(NewRot);
    if NewRot > 4 then
        NewRot:= 1;

    for B1:= 1 to 4 do
        for B2:= 1 to 4 do
            if PlayArea.Tetrominoes[PlayArea.CurrentTetromino.ShapeNum].Def[NewRot, b2, b1] then
                if PlayArea.Piece[PlayArea.CurrentTetromino.X + B1 - 1, PlayArea.CurrentTetromino.Y + B2 - 1] <> 0 then
                begin // colliding with another piece
                    Exit;
                end;

    // colliding with the walls
    OldRot:= PlayArea.CurrentTetromino.ShapeRot;
    PlayArea.CurrentTetromino.ShapeRot:= NewRot;
    if PlayArea.PieceRightSide + PlayArea.CurrentTetromino.X - 1 > PieceXTot then begin PlayArea.CurrentTetromino.ShapeRot:= OldRot; Exit; end; // colliding right
    if PlayArea.PieceLeftSide  + PlayArea.CurrentTetromino.X <= 1            then begin PlayArea.CurrentTetromino.ShapeRot:= OldRot; Exit; end; // colliding left
    if PlayArea.PieceDownSide  + PlayArea.CurrentTetromino.Y - 1 > PieceYTot then begin PlayArea.CurrentTetromino.ShapeRot:= OldRot; Exit; end; // colliding down
    if PlayArea.PieceUpSide    + PlayArea.CurrentTetromino.Y - 1 < 1         then begin PlayArea.CurrentTetromino.ShapeRot:= OldRot; Exit; end; // colliding up

    // not colliding, rotate
    PlayArea.CurrentTetromino.ShapeRot:= OldRot;
    for B1:= 1 to 4 do
        for B2:= 1 to 4 do
            if PlayArea.CurrentShapeFilledAt(B2, B1) then
                ClearLocation(PlayArea, B1 + PlayArea.CurrentTetromino.X - 1, B2 + PlayArea.CurrentTetromino.Y - 1);

    PlayArea.CurrentTetromino.ShapeRot:= NewRot;

    DrawShape(
        PlayArea,
        PlayArea.CurrentTetromino.ShapeNum,
        PlayArea.CurrentTetromino.ShapeRot,
        PlayArea.CurrentTetromino.X,
        PlayArea.CurrentTetromino.Y);
end;

procedure TForm1.RedrawPieces(const PlayArea: TPlayArea);
var
    X, X1: Word;
begin
    for X:= 1 to PieceXTot do
        for X1:= 1 to PieceYTot do
            if PlayArea.Piece[X,X1] = 0 then
                ClearLocation(PlayArea, X, X1)
            else
                DrawPiece(PlayArea, X, X1, PlayArea.Piece[X, X1]);
end;

procedure TForm1.SetArrowsForKeyboardInput(const PlayArea: TPlayArea);
begin
    PlayArea.PlayerInput
        .ClearBindings
        .BindKeyboardCodes(piLeft,        TInitializableList<Integer>.Create([VK_LEFT]))
        .BindKeyboardCodes(piRight,       TInitializableList<Integer>.Create([VK_RIGHT]))
        .BindKeyboardCodes(piRotate,      TInitializableList<Integer>.Create([VK_UP]))
        .BindKeyboardCodes(piDrop,        TInitializableList<Integer>.Create([VK_DOWN]))
        .BindKeyboardCodes(piHardDrop,    TInitializableList<Integer>.Create([VK_RETURN]))
        .BindKeyboardCodes(piPowerup,     TInitializableList<Integer>.Create([VK_NUMPAD0,VK_INSERT]));
end;

procedure TForm1.SetWASDForKeyboardInput(const PlayArea: TPlayArea);
begin
    PlayArea.PlayerInput
        .ClearBindings
        .BindKeyboardCodes(piLeft,        TInitializableList<Integer>.Create([Ord('A'), Ord('a')]))
        .BindKeyboardCodes(piRight,       TInitializableList<Integer>.Create([Ord('D'), Ord('d')]))
        .BindKeyboardCodes(piRotate,      TInitializableList<Integer>.Create([Ord('W'), Ord('w')]))
        .BindKeyboardCodes(piDrop,        TInitializableList<Integer>.Create([Ord('S'), Ord('s')]))
        .BindKeyboardCodes(piHardDrop,    TInitializableList<Integer>.Create([9]))
        .BindKeyboardCodes(piPowerup,     TInitializableList<Integer>.Create([Ord('1')]));
end;

procedure TForm1.CheckInput;
var
    PlayArea: TPlayArea;
begin
    for PlayArea in PlayAreas do
    begin
        if not PlayArea.GameOver then
        begin
            if not PlayArea.LineEvent then
            begin
                if PlayArea.PlayerInput.IsDrop then
                begin
                    if PlayArea.ReachedBottom then
                    begin
                        NewShape(PlayArea)
                    end
                    else begin
                        DownPiece(PlayArea);
                    end;
                end;

                if PlayArea.PlayerInput.IsLeft then
                begin
                    if PlayArea.DoesNotCollideLeft then
                    begin
                        LeftPiece(PlayArea);
                    end;
                end;

                if PlayArea.PlayerInput.IsRight then
                begin
                    if PlayArea.DoesNotCollideRight then
                    begin
                        RightPiece(PlayArea);
                    end;
                end;
            end;
        end;
    end;
end;

procedure TForm1.Clear2Lines(const PlayArea: TPlayArea);
var
    X, X1, R: Word;
begin
    for X:= PieceYTot downto PieceYTot - 1 do
    begin
        for X1:= 1 to PieceXTot do
            PlayArea.Piece[X1, X]:= 0;
        for X1:= X downto 2 do
            for R:= 1 to PieceXTot do
                PlayArea.Piece[R, X1] := PlayArea.Piece[R, X1 - 1];
    end;

    RedrawPieces(PlayArea);

    DrawShape(
        PlayArea,
        PlayArea.CurrentTetromino.ShapeNum,
        PlayArea.CurrentTetromino.ShapeRot,
        PlayArea.CurrentTetromino.X,
        PlayArea.CurrentTetromino.Y);
end;

procedure TForm1.ClearPlayAreas;
var
    PlayArea: TPlayArea;
begin
    for PlayArea in PlayAreas do
        PlayArea.Free;

    PlayAreas.Clear;
end;

function TForm1.SetupSingle: TPlayArea;
var
    ImageList: TInitializableList<TImage>;
    PlayArea: TPlayArea;
begin
    ClearPlayAreas;

    ImageList:= TInitializableList<TImage>.Create([Image1, Image2, Image3, Image4, Image5, Image6, Image7]);
    try
        PlayArea:= AddPlayArea(AreaP1, ScoreP1, '1UP', ImageList);
    finally
        ImageList.Free;
    end;

    SetArrowsForKeyboardInput(PlayArea);

    Result:= PlayArea;
end;

function TForm1.SetupDual: TPlayArea;
var
    ImageList: TList<TImage>;
    PlayArea1, PlayArea2: TPlayArea;
begin
    PlayArea1:= SetupSingle;
    SetWASDForKeyboardInput(PlayArea1);

    ImageList:= TInitializableList<TImage>.Create([Image3, Image4, Image5, Image6, Image7, Image1, Image2]);
    try
        PlayArea2:= AddPlayArea(AreaP2, ScoreP2, '2UP', ImageList);
    finally
        ImageList.Free;
    end;

    SetArrowsForKeyboardInput(PlayArea2);

    PlayArea1.Opponent:= PlayArea2;
    PlayArea2.Opponent:= PlayArea1;

    Result:= PlayArea2;
end;

procedure TForm1.InitRotatingMessage(const aMessage: String);
begin
    RotatingMessage:= aMessage;
    RotationDirection:= Random(2) * 2 - 1;
end;

procedure TForm1.InitScroller(const aMessage: String);
begin
    ScrollOffsetX:= ClientWidth;
    ScrollOffsetY:= ClientHeight - 25;
    ScrollMessage:= aMessage;

    ScrollDeg:= 0;
    ScrollTimer.Enabled:= True;
end;

procedure TForm1.InitSinusScroller(const aMessage: String);
var
    i: Integer;
begin
    SinusScrollMessage:= aMessage;
    SinusScrollOffsetX:= ClientWidth;
    SetLength(SinPoints, BlendedBitmap.Width + 1);

    for i:= 0 to BlendedBitmap.Width do
        SinPoints[i]:= round(sin((i / (pi * 20))) * (BlendedBitmap.Height / 4));

    ScrollTimer.Enabled:= True;
end;

procedure TForm1.Scroll;
var
    Scroller: TBitmap;
//    RotatingText: TBitmap;
    RotatingLogoBmp: TBitmap;
    i, n, p: Integer;
begin
    Scroller:= TBitmapFactory.GiveMeA24bitBitmap
                    .WithWidth(BlendedBitmap.Width)
                    .WithHeight(BlendedBitmap.Height);

//    RotatingText:= GiveMeA24bitBitmap;
    RotatingLogoBmp:= TBitmapFactory.GiveMeA24bitBitmap;

    try
        Scroller.Canvas.Font.Name:= TahomaFont;
        Scroller.Canvas.Font.Style:= [TFontStyle.fsBold];
        Scroller.Canvas.Font.Color:= clYellow;
        Scroller.Canvas.Brush.Style:= bsClear;

//        RotatingText.Width:= BlendedBitmap.Width;
//        RotatingText.Height:= BlendedBitmap.Height;
//        RotatingText.Canvas.Font.Name:= 'Tahoma';
//        RotatingText.Canvas.Font.Size:= 80;
//        RotatingText.Canvas.Font.Style:= [TFontStyle.fsBold];
//        RotatingText.Canvas.Font.Color:= clYellow;
//        RotatingText.Canvas.Brush.Style:= bsClear;
//
        BlendedBitmap.RenderOn(
            Scroller.Canvas.Handle,
            0,
            0,
            Scroller.Width,
            Scroller.Height);

//        BitBlt(
//            RotatingText.Canvas.Handle,
//            0,
//            0,
//            RotatingText.Width,
//            RotatingText.Height,
//            BlendedBitmap.Canvas.Handle,
//            0,
//            0,
//            SRCERASE);
//
//        RotatingText.Canvas.TextOut(0, 0, RotatingMessage);
//        RotatingText.Canvas.Font.Color:= clBlack;
//        RotatingText.Canvas.TextOut(5, 5, RotatingMessage);
//        RotatingText.Canvas.Font.Color:= clRed;
//        RotatingText.Canvas.TextOut(10, 10, RotatingMessage);

        ScrollDeg:= ScrollDeg + 1.5;

//        RotateBitmap(RotatingText, DegToRad(ScrollDeg) * RotationDirection, False);

//        BitBlt(
//            Scroller.Canvas.Handle,
//            0,
//            0,
//            RotatingText.Width,
//            RotatingText.Height,
//            RotatingText.Canvas.Handle,
//            0,
//            0,
//            SRCPAINT);

        RotatingLogoBmp.Assign(RotatingLogo);
        if ScrollDeg < ClientWidth then
            RotatingLogoBmp.Resize(Trunc(ScrollDeg), ClientHeight);

        RotatingLogoBmp
            .Rotate(
                DegToRad(ScrollDeg) * RotationDirection,
                False)
            .RenderTransparentOn(
                Scroller.Canvas,
                (Scroller.Width - RotatingLogoBmp.Width) div 2,
                (Scroller.Height - RotatingLogoBmp.Height) div 2,
                clBlack);

        //RotateBitmap(RotatingLogoBmp, DegToRad(ScrollDeg) * RotationDirection, False);

//        DrawTransparentBmp(
//            Scroller.Canvas,
//            (Scroller.Width - RotatingLogoBmp.Width) div 2,
//            (Scroller.Height - RotatingLogoBmp.Height) div 2,
//            RotatingLogoBmp,
//            clBlack);


        if not ScrollMessage.IsEmpty then
        begin
            Scroller.Canvas.Font.Size:= 14;
            Scroller.Canvas.Font.Color:= clYellow;

            Dec(ScrollOffsetX, 2);

            if ScrollOffsetX < -Scroller.Canvas.TextWidth(ScrollMessage) then
                ScrollOffsetX:= BlendedBitmap.Width;

            Scroller.Canvas.TextOut(
                ScrollOffsetX,
                ScrollOffsetY,
                ScrollMessage);
        end;

        if not SinusScrollMessage.IsEmpty then
        begin
            Scroller.Canvas.Font.Size:= 24;
            Scroller.Canvas.Font.Color:= clYellow;

            Dec(SinusScrollOffsetX);

            if SinusScrollOffsetX < -(Scroller.Canvas.TextWidth(SinusScrollMessage) * 1.2) then
            begin
                SinusScrollOffsetX:= BlendedBitmap.Width;
            end;

            n:= SinusScrollOffsetX;

            for i:= 0 to length(SinusScrollMessage) - 1 do
            begin
                if i + n < BlendedBitmap.Width then
                begin
                    Scroller.Canvas.TextOut(
                        i + n,
                        Scroller.Height div 2 + SinPoints[i + n],
                        SinusScrollMessage[i + 1]);
                end;
                Inc(n, Scroller.Canvas.TextWidth(SinusScrollMessage[i + 1]));
            end;
        end;

        Scroller.RenderOn(
            Self.Canvas.Handle,
            0,
            0,
            Self.Width,
            Self.Height);

    finally
//        RotatingText.Free;
        RotatingLogoBmp.Free;
        Scroller.Free;
    end;
end;

function TForm1.GetPowerupBitmap(const PlayArea: TPlayArea): TBitmap;
var
    powerup: Integer;
begin
    case PlayArea.ActivePowerup of
        puFlipHorizontal:   Result:= PowerupFlipHorizontal.Picture.Bitmap;
        puFlipVertical:     Result:= PowerupFlipVertical.Picture.Bitmap;
        puSpeedup:          Result:= PowerupSpeedup.Picture.Bitmap;
        puSlowdown:         Result:= PowerupSlowdown.Picture.Bitmap;
        puTimeFlies:        Result:= PowerupTimeFlies.Picture.Bitmap;
        puTimeFull:         Result:= PowerupTimeFull.Picture.Bitmap;
    end;
end;

procedure TForm1.RemovePowerups;
var
    PlayArea: TPlayArea;
begin
    for PlayArea in PlayAreas do
    begin
        PlayArea.ActivePowerup:= puNone;
        DrawScoreAndNextPiece(PlayArea);
    end;
end;

end.
